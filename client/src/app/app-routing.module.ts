import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BasicLoginComponent} from './components/basic-login/basic-login.component';
import {TestComponent} from './components/test/test.component';
import {DashboardDefaultComponent} from './components/dashboard-default/dashboard-default.component'
import {AdminComponent} from './components/admin/admin.component'

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {path:'login',component:BasicLoginComponent},
  {path:'test',component:TestComponent},
  {path:'dashboard',component:AdminComponent,
   children:[
    {
      path: '',
      redirectTo: 'main',
      pathMatch: 'full'
    },
    {
      path: 'main',
      loadChildren: () => import('./components/home/home.module').then(m => m.HomeModule)
    },
    { path: 'individual', loadChildren: () => import('./components/individual/individual.module').then(m => m.IndividualModule) },
    { path: 'dynamic', loadChildren: () => import('./components/dynamic/dynamic.module').then(m => m.DynamicModule) },
    { path: 'bookmarks', loadChildren: () => import('./components/bookmarks/bookmarks.module').then(m => m.BookmarksModule) },
   ]
},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
