import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subscriber, of } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
import { adal } from 'adal-angular';
import * as AuthenticationContext from 'adal-angular/lib/adal'

import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private localURL = 'http://localhost:4200';
  private _config: adal.Config;
  private _context: adal.AuthenticationContext;

  constructor(private http: HttpClient) {
    this._config = environment.adalConfig;
    this._context = new AuthenticationContext(this._config);
  }

  get config(): adal.Config {
    return this._config;
  }

  get context(): adal.AuthenticationContext {
    return this._context;
  }

  get isLogged(): boolean {
    const user = this._context.getCachedUser();
    const token = this._context.getCachedToken(this._config.clientId);
    return !!user && !!token;
  }

  public getReportsInGroup(groupId: string, token: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    }); // this._context is the ADAL AuthenticationContext object
    return this.http.get(`https://api.powerbi.com/v1.0/myorg/groups/${groupId}/reports`, { headers: headers });
  }

  getQlikTicket() {

    var username = "bi@loyco.onmicrosoft.com";// Username of PowerBI "pro" account - stored in config
    var password = "B28!xggS";// Password of PowerBI "pro" account - stored in config
    var clientId = "1c17fd67-c0f5-4ae5-a0d5-e04840931b75";// Applicaton ID of app registered via Azure Active Directory - stored in config

    var headerValue = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Access-Control-Allow-Headers': "*"
    })
    // return new Promise((resolve,reject)=>{ this.http.post('http://localhost:3033/api/token', {headers:headerValue }).subscribe(
    return new Promise((resolve, reject) => {
      this.http.post('/api/token', { headers: headerValue }).subscribe(
        (res: any) => {

          if (res.state == 1) {
            resolve(res.data);
          }
        },
        error => {
          console.log(error);

        }
      )
    })
  }

  getDatasetId(accessToken, groupId, reportId) {
    var headerValue = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Headers': "*",
      'accesstoken': accessToken
    })

    var body = {
      'groupId': groupId,
      'reportId': reportId
    }
    // return new Promise((resolve,reject)=>{ this.http.post('http://localhost:3033/api/getDatasetId',body, {headers:headerValue }).subscribe(
    return new Promise((resolve, reject) => {
      this.http.post('/api/getDatasetId', body, { headers: headerValue }).subscribe(
        (res: any) => {

          if (res.state == 1) {
            resolve(res.data);
          }
        },
        error => {
          console.log(error);

        }
      )
    })


  }


  getEmbedToken(accessToken, datasetIds, groupIds, reportIds, identities) {
    var headerValue = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Headers': "*",
      'accesstoken': accessToken
    })

    var body = {
      'groups': groupIds,
      'reports': reportIds,
      'identities': identities,
      'datasets': datasetIds
    }

    // return new Promise((resolve,reject)=>{ this.http.post('http://localhost:3033/api/getEmbedToken',body, {headers:headerValue }).subscribe(
    return new Promise((resolve, reject) => {
      this.http.post('/api/getEmbedToken', body, { headers: headerValue }).subscribe(
        (res: any) => {

          if (res.state == 1) {
            resolve(res.data);
          }
        },
        error => {
          console.log(error);

        }
      )
    })


  }

  getUserRole(userName) {
    var headerValue = new HttpHeaders({
      'Access-Control-Allow-Headers': "*",
      'username': userName
    })

    // return new Promise((resolve,reject)=>{ this.http.get('http://localhost:3033/api/getUserDetails', {headers:headerValue }).subscribe(
    return new Promise((resolve, reject) => {
      this.http.get('/api/getUserDetails', { headers: headerValue }).subscribe(
        (res: any) => {

          resolve(res);
        },
        error => {
          console.log(error);

        }
      )
    })
  }


  getFilter() {
    var body = {
      'username': sessionStorage.getItem('userEmail')

    }
    return new Promise((resolve, reject) => {
      this.http.post('/api/getFilter', body).subscribe(
        (res: any) => {

          resolve(res);
        },
        error => {
          console.log(error);

        }
      )
    })
  }



}






