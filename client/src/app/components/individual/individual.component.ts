import { Component, OnInit, ViewChild, ElementRef, } from '@angular/core';
import { AuthService } from "../../services/auth/auth.service"
import { environment } from '../../../environments/environment';
import { EmbedReportService } from "../../services/embed-report/embed-report.service"
import { Router } from '@angular/router';


@Component({
  selector: 'app-individual',
  templateUrl: './individual.component.html',
  styleUrls: ['./individual.component.css']
})
export class IndividualComponent implements OnInit {
  private pbiContainerElement: HTMLElement;
  public screenHeight: number;

  @ViewChild('reportContainer', { static: false }) embedreport1: ElementRef;


  constructor(private adalSrv: AuthService, private router: Router, private report: EmbedReportService) { }

  ngOnInit() {

    var userEmail = sessionStorage.getItem('userEmail');
    var userRole=sessionStorage.getItem('userRole');
    this.pbiContainerElement = <HTMLElement>(document.getElementById('embedreport1-container'));
    this.screenHeight = (window.screen.height);
    if (!userEmail) {
      alert('Error')
      this.router.navigate(['/login']);
    }
    else {
      var reportID;
      if(userRole=="Admin"){
        reportID=environment.reportIndividualAdmin;
      }
      else{
        reportID=environment.report3
      }

      this.adalSrv.getQlikTicket().then((value) => {
        this.report.showEmbedReport(value['access_token'], this.pbiContainerElement, reportID, null)
      }, (error) => {
        console.log(error);
      });


    }
  }

}
