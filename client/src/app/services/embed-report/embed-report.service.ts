import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import * as pbi from 'powerbi-client';
import { NgxPowerBiService } from 'ngx-powerbi'
import { Page } from 'page';
import { AuthService } from "../../services/auth/auth.service"
import { Router } from '@angular/router';
import { ISettings } from 'embed';

@Injectable({
  providedIn: 'root'
})
export class EmbedReportService {
  private powerBiService: NgxPowerBiService;
  constructor(private adalSrv: AuthService) {
    this.powerBiService = new NgxPowerBiService();

   }


  showEmbedReport(accessToken,pbiContainerElement,report,filter) {
    var updatedToken = "Bearer " + accessToken;
    const reportID=report;
    this.adalSrv.getDatasetId(updatedToken, environment.groupId, report).then((result) => {

      var data = JSON.parse(result['body'])
      var embedUrl = data.embedUrl;
      var datasetArray = [], reportArray = [], groupArray = [], identityArray = [];

      datasetArray.push({ id: data.datasetId });
      reportArray.push({ id: report });
      groupArray.push({ id: environment.groupId });
      identityArray.push({
        username: sessionStorage.getItem('userEmail'),
        datasets: [data.datasetId],
        reports: [report],
        roles: ["SecuiTy"],
        customData: null,
        identityBlob: null
      })
      this.adalSrv.getEmbedToken(updatedToken, datasetArray, groupArray, reportArray, identityArray).then((result) => {
        const embedToken = result['body'].token;
        console.log("filter test",filter)
        let flag:ISettings;
        let config = {
          type: 'report',
          id: reportID,
          embedUrl: embedUrl,
          accessToken: embedToken,
          tokenType: pbi.models.TokenType.Embed,
          permissions: pbi.models.Permissions.All,
          viewMode: pbi.models.ViewMode.View,
          filters:filter!=null?[filter]:[],
          settings: {
           //filterPaneEnabled: flag.,
          //  navContentPaneEnabled: false,
          }
        };
        let powerbi = new pbi.service.Service(pbi.factories.hpmFactory, pbi.factories.wpmpFactory, pbi.factories.routerFactory);  
        powerbi.reset(pbiContainerElement)
        console.log("config test",config)

    let report = powerbi.embed(pbiContainerElement,config);


    report.on('loaded', function(event) {
      console.log('report',report);
      if(window.location.href.indexOf('bookmarks')!=-1)
      {
        report['bookmarksManager'].getBookmarks().then(function (bookmarks) {
          console.log("bookmarks",bookmarks);
          
            if (sessionStorage.getItem('userRole')== 'Admin') {
                var currentBookmark = bookmarks[0];
            }
            else if (sessionStorage.getItem('userRole')== 'HR') {
                var currentBookmark = bookmarks[1];
            }
            // Apply the bookmarkstate
            report['bookmarksManager'].applyState(currentBookmark.state);
        });
      }
   
     });

      report.off('pageChanged');
     report.on<{ newPage: Page }>('pageChanged', event => {
       console.log('Page changed:', event.detail.newPage.displayName);
     });
      })

    })


  }
}
