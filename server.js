var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var login = require('./auth');
var path = require('path');
var cors = require('cors');
var app = express();

app.engine('html', require('ejs').renderFile);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
var router = express.Router();
// test route
router.get('/', function (req, res) {
    res.json({ message: 'welcome to our upload module apis' });
});

router.post('/token', login.getToken);
router.post('/getDatasetId', login.getDatasetId)
router.post('/getEmbedToken', login.getEmbedToken)
router.get('/snowflake', login.snowFlakeConnection)
router.get('/getUserDetails', login.getUserRole)
router.post('/getFilter', login.getFilter)
app.use(cors());

app.use('/api', router);

const clientAppPath = path.join(__dirname, './client/dist/powerbipoc');
app.use('/', express.static(clientAppPath));
app.use('/*', (req, res) =>
    res.status(200).sendFile(path.resolve(clientAppPath, 'index.html'))
);

app.listen('4200');
console.log("app running onn 4200");
