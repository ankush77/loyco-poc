// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiEndPoint: "https://login.microsoftonline.com/common/", // api endpoint for generationg embed tokens (for app-owns-data)
  powerBIEndpoint: "https://app.powerbi.com/reportEmbed?",
  groupId: "85557ebe-4fbe-4d6a-882e-714958d0511a", // similar to workspace id
  report1:"4baf66fa-aa8e-4797-9859-08c851ba0f86",
  report4:"16bc7c1e-799c-4c0d-a08f-086af2dde8f6",
  report3:"dcf68634-c7b0-4914-a091-27e6d72c8ecf",
  reportIndividualAdmin:'fb05cd18-bbcc-4f38-b206-44c4ce9f95d5',
  reportBookmark:"e90d9615-a0b0-48af-8e3b-c0caa45fd188",
  adalConfig: {
    tenant: '', //tenant id of your organization
    clientId: '1c17fd67-c0f5-4ae5-a0d5-e04840931b75', // client id of your azure ad application
    cacheLocation: 'localStorage', // Default is sessionStorage
    redirectUri:`${window.location.origin}/`    ,
    popUp: false    
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
