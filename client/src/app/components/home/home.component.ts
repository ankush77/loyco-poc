import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { AuthService } from "../../services/auth/auth.service"
import { environment } from '../../../environments/environment';
import {EmbedReportService} from "../../services/embed-report/embed-report.service"
import { NgxPowerBiService } from 'ngx-powerbi'
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  private powerBiService: NgxPowerBiService;
  private pbiContainerElement: HTMLElement;
  @ViewChild('reportContainer', { static: false }) reportContainer: ElementRef;

  public screenHeight: number;
  constructor(private adalSrv: AuthService,private router: Router,private report:EmbedReportService) {
    this.powerBiService = new NgxPowerBiService();
  }


  ngOnInit() {
    var userEmail=sessionStorage.getItem('userEmail')
    this.pbiContainerElement = <HTMLElement>(document.getElementById('pbi-container'));
    this.screenHeight = (window.screen.height);
    var that = this;
    if(!userEmail){
      alert('Error')
      this.router.navigate(['/login']);
  }
  else{

    this.adalSrv.getQlikTicket().then((value) => {     
      this.report.showEmbedReport(value['access_token'],this.pbiContainerElement,environment.report1,null)
    }, (error) => {
      console.log(error);
    });
    

  }

}


  ngAfterViewInit() {

}
}
