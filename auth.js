var request = require('request');
var querystring = require('querystring');
var snowflake = require('snowflake-sdk');
var usersData = require('./users.json');
var username = "", password = "", grant_type = "", clientId = "1c17fd67-c0f5-4ae5-a0d5-e04840931b75", client_secret = "", group_id = '85557ebe-4fbe-4d6a-882e-714958d0511a', report_id = "4baf66fa-aa8e-4797-9859-08c851ba0f86";

let token;

//snowflake configs

// var connection = snowflake.createConnection({
//   account: 'loyco.switzerland-north.azure',
//   username: 'bi@loyco.onmicrosoft.com',
//   password: 'B28!xggS'
// }
// );

// connection.connect(
//   function (err, conn) {
//     if (err) {
//       console.error('Unable to connect: ' + err.message);
//     }
//     else {
//       console.log('Successfully connected to Snowflake.');
//       // Optional: store the connection ID.
//       connection_ID = conn.getId();
//       console.log('Connection ID:', connection_ID);

//     }
//   }
// );

// snowflake ends
exports.getToken = function (req, res) {
  var formData = {
    grant_type: "password",
    client_id: clientId,
    resource: "https://analysis.windows.net/powerbi/api",
    scope: "openid",
    username: 'bi@loyco.onmicrosoft.com',
    password: 'zwiuP8N@',
  };
  var formattedBody = querystring.stringify(formData);
  var contentLength = formattedBody.length;
  var options = {
    // uri: "https://login.microsoftonline.com/organizations/oauth2/v2.0/token",
    uri: 'https://login.microsoftonline.com/common/oauth2/token',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    method: "POST",
    body: formattedBody,
  };

  request(options, function (error, response, body) {
    if (error) {
      console.log('Error Sumit: ' + error);
      //console.log(response);
      return res.json({ state: -1, message: error, data: null });
    }
    else {
      console.log("== Got a ticket ==");
      var finaOutput = JSON.parse(response.body)
      token = finaOutput.access_token;
      return res.json({ state: 1, message: "Success", data: finaOutput });

    }
  });

}


exports.getDatasetId = function (req, res) {

  //console.log("Test Request",JSON.stringify(req.body));
  var options = {
    // uri: "https://login.microsoftonline.com/organizations/oauth2/v2.0/token",
    uri: 'https://api.powerbi.com/v1.0/myorg/groups/' + req.body.groupId + '/reports/' + req.body.reportId,
    headers: {
      'Authorization': req.headers.accesstoken,

    },
    method: "GET",
  };
  request(options, function (error, response, body) {
    if (error) {
      console.log('Error Dataset: ' + error);
      //console.log(response);
      return res.json({ state: -1, message: error, data: null });
    }
    else {
      //console.log("== Got a ticket ==");
      // var finaOutput = JSON.parse(response.body)    
      //console.log(response);      
      return res.json({ state: 1, message: "Success", data: response });


    }
  });

}


exports.getEmbedToken = function (req, res) {
  var formData = {
    'datasets': req.body.datasets,
    'identities': req.body.identities,
    'reports': req.body.reports,
    'targetWorkspaces': req.body.groups,
  };
  // var formattedBody = querystring.stringify(formData);
  console.log("EmbedToken Angular", formData);

  var options = {
    // uri: "https://login.microsoftonline.com/organizations/oauth2/v2.0/token",
    uri: 'https://api.powerbi.com/v1.0/myorg/GenerateToken',
    headers: {
      'Authorization': req.headers.accesstoken,
      'Content-Type': 'application/json'

    },
    body: formData,
    method: "POST",
    json: true
  };

  request(options, function (error, response, body) {
    if (error) {
      console.log('Error embed: ' + error);
      //console.log(response);
      return res.json({ state: -1, message: error, data: null });
    }
    else {
      // console.log("== Got a embed Token ==", response);

      return res.json({ state: 1, message: "Success", data: response });


    }
  });

}

exports.snowFlakeConnection = function (req, res) {
  console.log("Into API");
  connection.connect(
    function (err, conn) {
      if (err) {
        console.error('Unable to connect: ' + err.message);
      }
      else {
        console.log('Successfully connected to Snowflake.');
        // Optional: store the connection ID.
        connection_ID = conn.getId();
        console.log('Connection ID:', connection_ID);

      }
    }
  );
  let username1 = 'bi@loyco.onmicrosoft.com';
  let query = 'Select * from "LY_POWBI_PROD"."PUBLIC"."LY_USER_ROLE" where USERNAME=' + "'" + username1 + "'";
  console.log("Query is:", query);
  connection.execute({
    sqlText: query,
    complete: function (err, stmt, rows) {
      if (err) {
        console.error('Failed to execute statement due to the following error: ' + err.message);
        res.json({ state: -1, message: "User Doesn't Exists" })
      } else {
        console.log('Successfully executed statement: ', rows[0]);
        res.json({ state: 1, message: "Success", data: rows })
      }
    }
  });
}
//merged
//userdetails added
exports.getUserRole = function (req, res) {
  var userName = req.headers.username;
  // var index = usersData.find(p => p.USERNAME == userName);
  // //Index Value: { username: 'bi@loyco.onmicrosoft.com', role: 'Admin' }
  // if (index == undefined) {
  //   return res.json({ state: -1, message: "User Doesn't Exists" })
  // }
  // else {
  //   console.log("Index Value:", index);
  //   return res.json({ state: 1, message: "Success", data: index })
  // }

  var connection = snowflake.createConnection({
    account: 'loyco.switzerland-north.azure',
    username: 'bi@loyco.onmicrosoft.com',
    password: 'B28!xggS'
  }
  );
  connection.connect(
    function (err, conn) {
      if (err) {
        console.error('Unable to connect: ' + err.message);
        res.json({ state: -1, message: err.message })
      }
      else {
        console.log('Successfully connected to Snowflake.');
        // Optional: store the connection ID.
        connection_ID = conn.getId();
        console.log('Connection ID:', connection_ID);

        let query = 'Select * from "LY_POWBI_PROD"."PUBLIC"."LY_USER_ROLE" where USERNAME=' + "'" + userName + "'";
        connection.execute({
          sqlText: query,
          complete: function (err, stmt, rows) {
            if (err) {
              console.error('Failed to execute statement due to the following error: ' + err.message);
              res.json({ state: -1, message: err.message })
            } else {
              console.log('Successfully executed statement: ', rows);
              connection.destroy(function (err, conn) {
                if (err) {
                  console.error('Unable to disconnect: ' + err.message);
                } else {
                  console.log('Disconnected connection with id: ' + connection.getId());
                }
              });
              rows.length == 0 ? res.json({ state: -1, message: "User Doesn't Exists" }) : res.json({ state: 1, message: "Success", data: rows[0] })
            }
          }
        });
      }
    }
  );
}

//get Filter values
exports.getFilter = function (req, res) {
  var userName = req.body.username;
  console.log("Username for filter is:", userName);
  var connection = snowflake.createConnection({
    account: 'loyco.switzerland-north.azure',
    username: 'bi@loyco.onmicrosoft.com',
    password: 'B28!xggS'
  }
  );
  connection.connect(
    function (err, conn) {
      if (err) {
        console.error('Unable to connect: ' + err.message);
        res.json({ state: -1, message: err.message })
      }
      else {
        console.log('Successfully connected to Snowflake.');
        // Optional: store the connection ID.
        connection_ID = conn.getId();
        console.log('Connection ID:', connection_ID);

        // let query = 'Select distinct SETID from "LY_POWBI_PROD"."PUBLIC"."LY_COMPANY"';
        let query = 'Select distinct SUBSTRING(EMPLID,1,2) as SETID from "LY_POWBI_PROD"."PUBLIC"."LY_SECURITY2" where lower(USERID) =' + "'" + userName + "'";

        connection.execute({
          sqlText: query,
          complete: function (err, stmt, rows) {
            if (err) {
              console.error('Failed to execute statement due to the following error: ' + err.message);
              res.json({ state: -1, message: err.message })
            } else {
              console.log('Successfully executed statement: ', rows);
              connection.destroy(function (err, conn) {
                if (err) {
                  console.error('Unable to disconnect: ' + err.message);
                } else {
                  console.log('Disconnected connection with id: ' + connection.getId());
                }
              });
              res.json({ state: 1, message: "Success", data: rows })
            }
          }
        });
      }
    }
  );
}
