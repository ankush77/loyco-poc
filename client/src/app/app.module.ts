import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgxPowerBiModule} from 'ngx-powerbi'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestComponent } from './components/test/test.component';
import { HttpClientModule } from '@angular/common/http';
import { BasicLoginComponent } from './components/basic-login/basic-login.component';
import {SharedModule} from './shared/shared.module';
import { AdminComponent } from './components/admin/admin.component';
import {TitleComponent} from './components/admin/title/title.component';
import {BreadcrumbsComponent} from './components/admin/breadcrumbs/breadcrumbs.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    BasicLoginComponent,
    AdminComponent,
    TitleComponent,
    BreadcrumbsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxPowerBiModule,
    SharedModule,
    FontAwesomeModule,
    NgMultiSelectDropDownModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
