import { TestBed } from '@angular/core/testing';

import { EmbedReportService } from './embed-report.service';

describe('EmbedReportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EmbedReportService = TestBed.get(EmbedReportService);
    expect(service).toBeTruthy();
  });
});
