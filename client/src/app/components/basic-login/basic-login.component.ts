import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth/auth.service'

@Component({
  selector: 'app-basic-login',
  templateUrl: './basic-login.component.html',
  styleUrls: ['./basic-login.component.css']
})
export class BasicLoginComponent implements OnInit {
  @ViewChild('userEmail', { static: true }) userEmail: ElementRef;
  constructor(private router: Router, private auth: AuthService) { }

  ngOnInit() {
    document.querySelector('body').setAttribute('themebg-pattern', 'theme1');
  }
  getLoggedIn() {
    let inputValue = this.userEmail.nativeElement.value;
    if (!inputValue) {
      alert("Please enter a valid user email")
    }
    else {

      this.auth.getUserRole(inputValue).then((value) => {
        if (value['state'] != 1) {
          alert(value['message'])
        }
        else {
          sessionStorage.setItem('userEmail', value['data']['USERNAME']);
          sessionStorage.setItem('userRole', value['data']['ROLE'])
          this.gotoDashboard();
        }

      })
    }
  }



  gotoDashboard() {
    this.router.navigate(['/dashboard']);
  }

}

