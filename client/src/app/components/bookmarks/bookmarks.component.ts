import { Component, OnInit, ViewChild, ElementRef, } from '@angular/core';
import { AuthService } from "../../services/auth/auth.service"
import { environment } from '../../../environments/environment';
import { EmbedReportService } from "../../services/embed-report/embed-report.service"
import { Router } from '@angular/router';

@Component({
  selector: 'app-bookmarks',
  templateUrl: './bookmarks.component.html',
  styleUrls: ['./bookmarks.component.css']
})
export class BookmarksComponent implements OnInit {
  private pbiContainerElement: HTMLElement;
  public screenHeight: number;

  constructor(private adalSrv: AuthService, private router: Router, private report: EmbedReportService) { }

  ngOnInit() {
    var userEmail = sessionStorage.getItem('userEmail')
    this.pbiContainerElement = <HTMLElement>(document.getElementById('bookmark-container'));
    this.screenHeight = (window.screen.height);
    if (!userEmail) {
      alert('Error')
      this.router.navigate(['/login']);
    }
    else {
      
      this.adalSrv.getQlikTicket().then((value) => {
        this.report.showEmbedReport(value['access_token'], this.pbiContainerElement, environment.reportBookmark, null)
      }, (error) => {
        console.log(error);
      });


    }
  }

}
